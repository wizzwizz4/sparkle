use graphics;

mod display;

fn main() {
    let mut g = display::Display::new();
    loop {
        g.frame().commit().unwrap();
    }
}
